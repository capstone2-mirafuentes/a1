const mongoose = require('mongoose')

const orderSchema = {
	userId: {
		type: String,
		required: [true, "User id is required"]
	},
	products: [{
		productId: {
			type: String

		},
		price: {
			type: Number

		},
		quantity: {
			type: Number

		}
	}],
	totalAmount: {
		type: Number,
		default: 0

	},
	puchasedOn: {
		type: Date,
		default: new Date()
	}
}
module.exports = mongoose.model("Order", orderSchema)