const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Firstname is required"]
	},
	password: {
		type: String,
		required: [true, "Firstname is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
})

module.exports = mongoose.model('User', userSchema)