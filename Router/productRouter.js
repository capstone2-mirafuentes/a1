const express = require('express')
const auth = require('../auth.js')
const productController = require("../Controllers/productController.js")
const router = express.Router()

//router for creating product
router.post('/createProduct', auth.verify, productController.createProduct)

//router
router.get("/", productController.allProduct)

//router get
router.get("/all", auth.verify, productController.All)

//router for retrieving single product
router.get("/:productId", productController.getSingleProduct)

//router for updating product
router.put("/update/:productId", auth.verify, productController.updateProduct)

//router for achiving product
router.put("/archive/:productId", auth.verify, productController.archiveProduct)

router.put("/activate/:productId", auth.verify, productController.activateProduct)

module.exports = router