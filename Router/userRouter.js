const express = require('express')
const userController = require('../Controllers/UserController.js')
const router = express.Router()
const auth = require('../auth.js')

//router for user registration
router.post('/registration', userController.userRegistration)

//router for user authentication
router.post("/authentication", userController.userAuthentication)

//router for get all profile
router.get("/getProfile", auth.verify, userController.getProfile)

//router
router.get("/profile", auth.verify, userController.Profile)

router.get("/login", auth.verify, userController.Profile)

//router for user to admin
router.put("/userToAdmin/:userId", userController.userToAdmin)








module.exports = router