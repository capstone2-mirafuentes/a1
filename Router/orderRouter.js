const express = require('express')
const auth = require('../auth.js')
const orderController = require('../Controllers/orderController.js')
const router = express.Router()

//router for creating order
router.post('/createOrder', auth.verify, orderController.createOrder)
//router for retrieving user's orders
router.get("/userOrder", auth.verify, orderController.getUserOrders)
//router for retrieving all orders
router.get("/allOrder", auth.verify, orderController.retrieveAllOrders)

//router for add cart
router.post('/addCart/:productId', auth.verify, orderController.addCart)
//router for change quanttity



module.exports = router