const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const userRouter = require("./Router/userRouter.js")
const productRouter = require("./Router/productRouter.js")
const orderRouter = require("./Router/orderRouter.js")

const port = 4000


const app = express()

//mongoDB connection
mongoose.connect("mongodb+srv://admin:admin@batch245-mirafuentes.m5uejs3.mongodb.net/capstone2_mirafuentes?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
let db = mongoose.connection

//error handling
db.on('error', console.error.bind(console, "Connection error"))

//Success
db.once('open', () => {console.log("We are connected to the cloud")})




//middlewares
app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors())

//routers
app.use("/user", userRouter)
app.use("/product", productRouter)
app.use("/order", orderRouter)

app.listen(port, console.log(`Server is running at port ${port}`))