const mongoose = require('mongoose')
const Product = require('../Models/productSchema.js')
const auth = require('../auth.js')
const User = require("../Models/userSchema.js")

//create product
module.exports.createProduct = (request, response) => {
	let input = request.body
	let userData = auth.decode(request.headers.authorization)
	User.findOne({isAdmin: userData.isAdmin})
	.then(result => {
		if(result.isAdmin === false) {
			return response.send(false)
		}else{
			let newProduct = new Product({
				name: input.name,
				description: input.description,
				price: input.price
			})
			newProduct.save()
			.then(save => response.send(true))
			.catch(error => response.send(error))
		}
	})
	.catch(error => response.send(error))
}

//get all products
module.exports.allProduct = (request, response) => {
	Product.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error))
}

//get all products via admin
module.exports.All = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	if(userData.isAdmin === true){
		Product.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}else{
		return response.send(false)
	}
	
}

//get single product
module.exports.getSingleProduct = (request, response) => {
	const productId = request.params.productId
	Product.findById(productId)
	.then(result => response.send(result))
	.catch(error => response.send(error))
}

//update specific product
module.exports.updateProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	let input = request.body
	const productId = request.params.productId
	User.findOne({isAdmin: userData.isAdmin})
	.then(result => {
		if(result.isAdmin === true){
			let updateProduct = {
				name: input.name,
				description: input.description,
				price: input.price
			}
			Product.findByIdAndUpdate(productId, updateProduct, {new: true})
			.then(result => response.send(result))
			.catch(error => response.send(error))
			
		}else{
			return response.send(false)
		}
	})
	.catch(error => response.send(error))
}

//archive specific product
module.exports.archiveProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	const productId = request.params.productId
	const input = request.body
	User.findOne({isAdmin: userData.isAdmin})
	.then(result => {
		if(result.isAdmin === true){
			let updateProduct = {
				isActive: false
			}
			Product.findByIdAndUpdate(productId, updateProduct, {new: true})
			.then(result => response.send(result))
			.catch(error => response.send(error))
		}else{
			return response.send(true)
		}
	})
	.catch(error => response.send(error))
}

module.exports.activateProduct = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	const productId = request.params.productId
	const input = request.body
	User.findOne({isAdmin: userData.isAdmin})
	.then(result => {
		if(result.isAdmin === true){
			let updateProduct = {
				isActive: true
			}
			Product.findByIdAndUpdate(productId, updateProduct, {new: true})
			.then(result => response.send(result))
			.catch(error => response.send(error))
		}else{
			return response.send(false)
		}
	})
	.catch(error => response.send(error))
}