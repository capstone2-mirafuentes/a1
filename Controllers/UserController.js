const mongoose = require('mongoose')
const User = require("../Models/userSchema.js")
const bcrypt = require('bcrypt')
const auth = require('../auth.js')

module.exports.userRegistration = (request, response) => {
	let input = request.body
	User.findOne({email: input.email})
	.then(result => {
		if(result !== null){
			return response.send(false)
		}else{
			let newUser = new User({
				email: input.email,
				password: bcrypt.hashSync(input.password, 10)
			})
			newUser.save()
			.then(save => response.send(true))
			.catch(error => response.send(error))
		}
	})
	.catch(error => {
		return response.send(error)
	})
}

module.exports.userAuthentication = (request, response) => {
	let input = request.body
	User.findOne({email: input.email})
	.then(result => {
		if(result === null){
			return response.send(false)
		}else{
			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)
			if(isPasswordCorrect){
				return response.send({auth: auth.createAccessToken(result)})
			}else{
				return response.send(false)
			}
		}
	})
	.catch(error => response.send(error))
}

module.exports.getProfile = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	if(userData.isAdmin === false){
		return response.send(false)
	}
	else{
		User.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}
}

module.exports.login = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
		User.find({userData})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}

module.exports.userToAdmin = (request, response) => {
	const input = request.body
	const userId = request.params.userId

	User.findById(userId)
	.then(result => {
		if(result.isAdmin === true){
			return response.send("you are already an admin")
		}
		else{
			let updateUser = {
				isAdmin: input.isAdmin
			}
			User.findByIdAndUpdate(userId, updateUser, {new: true})
			.then(result => response.send(result))
			.catch(error => response.send(error))
		}
	})
	.catch(error => response.send(error))
}

module.exports.Profile = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	User.find(userData)
	.then(result => response.send(result))
	.catch(error => response.send(error))
}