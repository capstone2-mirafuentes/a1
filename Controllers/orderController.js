const mongoose = require('mongoose')
const Order = require('../Models/orderSchema.js')
const auth = require('../auth.js')
const User = require("../Models/userSchema.js")
const Product = require('../Models/productSchema.js')

module.exports.createOrder = async (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	let input = request.body

	if(userData.isAdmin === true){
		return response.send(false)
	}
	else{
		await Order.findOne({userId: userData._id})
		.then(result => {
			if(result !== null){
				return response.send(false)
			}
			else{			
				let newOrder = new Order({
					userId: userData._id,	
				})
				newOrder.save()
				.then(save => response.send(save))
				.catch(error => response.send(error))
			}
		})
		.catch(error => response.send(error))
	}
}
module.exports.addCart = async (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	const input = request.body
	const productId = request.params.productId
	const productPrice = await Product.findById(productId)

	await Order.findOne({userId: userData._id})
	.then(result => {
		if(result.userId !== userData._id) {
			return response.send("error")
		}
		else {
			result.products.push({productId: productId._id, quantity: input.quantity, price: productPrice.price})
			let subTotals = result.products.map(product => product.price * product.quantity)
			let productTotalAmount = subTotals.reduce((a,b) => (a+b))
			result.totalAmount = productTotalAmount				
			return result.save()
			.then(save => response.send("Add cart successfully"))
			.catch(error => response.send(error))
		}
	})
	.catch(error => response.send(error))
}

module.exports.getUserOrders = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	Order.find({userId: userData._id})
	.then(result => {
		return response.send(result)
	})
	.catch(error => response.send(error))
}

module.exports.retrieveAllOrders = (request, response) => {
	const userData = auth.decode(request.headers.authorization)
	User.findOne({isAdmin: userData.isAdmin})
	.then(result => {
		if(result.isAdmin === false){
			return response.send(false)
		}
		else{
			Order.find({})
			.then(result => response.send(result))
			.catch(error => response.send(error))
		}
	})
	.catch(error => response.send(error))
}
